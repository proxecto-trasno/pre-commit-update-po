import os
from argparse import ArgumentParser
from subprocess import run
from sys import argv, exit


def main():
    parser = ArgumentParser()
    parser.add_argument("-i", "--input", action="append")
    parser.add_argument("-l", "--language", action="append")
    parser.add_argument("-t", "--template")
    parser.add_argument("--keep-template", action="store_true")
    parser.add_argument("-o", "--output-template")
    parser.add_argument('MODIFIED_FILES', nargs='*')
    args = parser.parse_args()

    run(
        [
            "pybabel",
            "extract",
            *args.input,
            "-o",
            args.template,
        ]
    )
    for language in args.language:
        po_file = args.output_template.format(language=language)
        run(
            [
                "pybabel",
                "update",
                "-i",
                args.template,
                "-l",
                language,
                "-o",
                po_file,
            ]
        )
        result = run(
            ["git", "diff", "--shortstat", po_file],
            capture_output=True,
        )
        if result.stdout == b" 1 file changed, 1 insertion(+), 1 deletion(-)\n":
            run(["git", "restore", po_file])

    if not args.keep_template:
        os.unlink(args.template)
